#!/bin/sh

set -e

distro=$1
rel=$2
timestamp=$3
vendor=$4

echo "${distro} ${rel} ${timestamp} ${vendor}" > /etc/image_version
