#!/bin/sh

set -eu

SRCLIST=/etc/apt/sources.list
MIRROR=
RELEASE=

opts=$(getopt -o "m:r:" -l "mirror:,release:" -- "$@")
eval set -- "$opts"

while [ $# -gt 0 ]; do
    case $1 in
        -m|--mirror) MIRROR="$2"; shift 2;;
        -r|--release) RELEASE="$2"; shift 2;;
        --) shift; break;;
        *) ;;
    esac
done

if [ -z "$MIRROR" ] || [ -z "$RELEASE" ] || [ $# -eq 0 ]; then
    echo "Please provive mirror, release and distribution(s)." >&2
    exit 1
fi

for dist in $@; do
    if ! grep -q "^deb .*$MIRROR.*$RELEASE.*$dist" $SRCLIST; then
        echo "deb $MIRROR $RELEASE $dist" >> $SRCLIST
    fi

    if ! grep -q "^deb-src .*$MIRROR.*$RELEASE.*$dist" $SRCLIST; then
        echo "deb-src $MIRROR $RELEASE $dist" >> $SRCLIST
    fi
done